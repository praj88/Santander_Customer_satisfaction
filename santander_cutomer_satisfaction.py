import pandas as pd
import numpy as np
from sklearn.cross_validation import cross_val_score
from sklearn.model_selection import GridSearchCV
from sklearn.externals import joblib
import matplotlib.pyplot as plt
from sklearn import preprocessing
%matplotlib inline
import xgboost as xgb
from xgboost.sklearn import XGBClassifier
from sklearn.metrics import accuracy_score, recall_score, precision_score, roc_curve, auc
import os
os.chdir('/app-santander-satisfaction')
from config import *
#<<<<<<<<<<<<<<<<<< Data Preprocessig >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

def pre_process(path):
    data_df = pd.read_csv(path)
    data_df_scaled = pd.DataFrame(preprocessing.scale(data_df[data_df.columns[1:-2]]))
    data_df_scaled = data_df_scaled.merge(data_df[['ID','TARGET']], left_index=True, right_index=True, how='outer')
    # be used as the training data and some as the test data.
    data_df_scaled['is_train'] = np.random.uniform(0, 1, len(data_df_scaled)) <= .80
    # Create two new dataframes, one with the training rows, one with the test rows
    train, test = data_df_scaled[data_df_scaled['is_train']==True], data_df_scaled[data_df_scaled['is_train']==False]
    # Show the number of observations for the test and training dataframes
    print('Number of observations in the training data:', len(train))
    print('Number of observations in the test data:',len(test))
    return train, test

def pre_process_predict(path_test):
    data_df_test = pd.read_csv(path_test)
    data_df_scaled_test = pd.DataFrame(preprocessing.scale(data_df_test[data_df_test.columns[1:]]))
    test_predict_df = pd.concat([data_df_scaled_test, data_df_test['ID']], axis=1)

    return test_predict_df


#<<<<<<<<<<<<<<<<<< Test >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

def test_model(test_features, model):
    preds = model.predict(test_features)
    return preds

def grid_search(param_grid, model):
    # Instantiate the grid search model
    grid_search = GridSearchCV(estimator = model, param_grid = param_grid,
                              cv = 5, n_jobs = -1, verbose = 2)

    # Fit the grid search to the data
    grid_search.fit(train_features, train_labels)

    print(grid_search.best_params_)
    print(grid_search.best_score_ )
    best_grid = grid_search.best_estimator_
    return best_grid

def plot_roc(test_labels, predsXGB):
        false_positive_rate, true_positive_rate, thresholds = roc_curve(test_labels, predsXGB)
        roc_auc = auc(false_positive_rate, true_positive_rate)

        plt.title('Receiver Operating Characteristic')
        plt.plot(false_positive_rate, true_positive_rate, 'b', label='AUC = %0.2f'% roc_auc)
        plt.legend(loc='lower right')
        plt.plot([0,1],[0,1],'r--')
        plt.xlim([-0.1,1.2])
        plt.ylim([-0.1,1.2])
        plt.ylabel('True Positive Rate')
        plt.xlabel('False Positive Rate')
        plt.show()


def model_metrics(preds):

    predsXGB_df = pd.DataFrame(preds)

    confusion_matrix_XGB = pd.crosstab( preds, test['TARGET'], rownames=['Predicted'], colnames=['Actual'])
    print(confusion_matrix_XGB)
    accuracy = accuracy_score(test_labels, preds)
    recall = recall_score(test_labels, preds)
    precision = precision_score(test_labels, preds)
    print("Accuracy:{}".format(accuracy))
    print("Recall:{}".format(recall))
    print("Precision:{}".format(precision))


#<<<<<<<<<<<<<<<<<< Main Function >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if __name__ == "__main__":

    train, test = pre_process(path)
    #<<<<<<<<<<<<<<<<<< Feature Extraction >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>›
    # Create a list of the feature column's names
    features = train.columns[0:-3]
    train_features= train[features]
    test_features= test[features]
    # Trainign Labels
    train_labels = train['TARGET']
    test_labels  = test['TARGET']

   #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< XGB >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    ratio = float(np.sum(train_labels==0))/np.sum(train_labels==1)
    xgb_cla = XGBClassifier(learning_rate =0.1,
                            n_estimators=100,
                            max_depth=5,
                            min_child_weight=1,
                            gamma=0,
                            subsample=0.8,
                            colsample_bytree=0.8,
                            objective= 'binary:logistic',
                            nthread=4,
                            scale_pos_weight=ratio,
                            seed=27)


    #xgb_cla.fit(train_features, train_labels)
    #inputs for the grid search
    param_grid_XGB = {
            'max_depth':[3,5],
            'min_child_weight':[1,4],
            'scale_pos_weight':[ratio,ratio/2]}

    # best model from grid search using CV
    best_model_XGB = grid_search(param_grid_XGB, xgb_cla)


    # test model on hold out daat set
    predsXGB = test_model(test_features, best_model_XGB)
    model_metrics(predsXGB)
    # Create a trained model file
    joblib.dump(xgb_cla, '/app-santander-satisfaction/best_model.pkl')


    test_predict = pre_process_predict(path_test)
    test_features_predict  = test_predict[features]
    # test model on hold out daat set
    predsXGB_predict = test_model(test_features_predict, best_model_XGB)
    predsXGB_predict_df = pd.DataFrame(predsXGB_predict)
    predsXGB_predict_df.columns = ['TARGET']
    predsXGB_predict_df = pd.concat((test_predict['ID'],predsXGB_predict_df), axis=1)
    predsXGB_predict_df.to_csv('/app-santander-satisfaction/submission.csv', index = False)
    print('Process Complete. Pease see submission file for predictions')
